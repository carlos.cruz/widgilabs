import { Component } from "@wordpress/element";
import { TextControl, SelectControl, PanelBody } from "@wordpress/components";
import { InspectorControls } from "@wordpress/editor";
import {__} from "@wordpress/i18n";

/**
 * Styles
 */
import './style.editor.scss'

class LatestPosts extends Component {
  onChangeEventType = (value) => {
    this.props.setAttributes( { type_event: value } )
  }

  onChangeEventId = (value) => {
    this.props.setAttributes( { event_id: value } )
  }

  render() {
    const { attributes } = this.props;

    return (
      <>
        <InspectorControls key={'settings'}>
          <PanelBody title={__('Configurações', 'widglabs')}>
            <SelectControl
              label={__('Exibir como', 'widglabs')}
              value={ attributes.type_event }
              options={ [
                { label: 'Lista de Eventos', value: 'all' },
                { label: 'Evento Único', value: 'unique' },
              ] }
              help={ __('Selecione um modo de exibição do evento.', 'agendalx-list') }
              onChange={ this.onChangeEventType }
            />

            { attributes.type_event === "unique"
              ? <TextControl
                type={'number'}
                label={__('ID do Evento', 'widgilabs')}
                value={attributes.event_id}
                help={ __('Selecione ID específico de um evento. ', 'agendalx-list') }
                onChange={ this.onChangeEventId }/>
              : null
            }

          </PanelBody>
        </InspectorControls>
        <div className={'widgilabs-block-events'}>
          <h3 className={'widgilabs-block-events-title'}>Agenda Cultural Lisboa</h3>

          <div className={'widgilabs-block-events-copy'}>
            <img src='https://www.widgilabs.com/wp-content/themes/widgilabs_2019/images/logoa.svg' alt="WidgiLabs" className={'widgilabs-block-events-img'}/>
          </div>
        </div>
      </>
    )
  }
}

export default LatestPosts
