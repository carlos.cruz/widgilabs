import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import edit from './edit'

registerBlockType('widgilabs/events', {
  title: __('Widglabs Eventos','widgilabs'),
  description: __('Lista os eventos da Agenda Cultural de Lisboa.','widgilabs'),
  category: 'layout',
  keywords: [
    __('widgilabs', 'widgilabs'),
    __('list', 'widgilabs'),
    __('events', 'widgilabs'),
    __('latest', 'widgilabs')
  ],
  icon: {
    background: '#fff',
    foreground: '#287e7e',
    src: 'list-view'
  },
  edit: edit,
  save() {
    return null
  }
})
