<?php
/**
 * Plugin Name:       Widgilabs
 * Description:       List the future events using Gutenberg Blocks with API - https://agendalx.pt/api
 * Requires at least: 5.7
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            Carlos@Widgilabs
 * License:           GPL-2.0
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       widglabs
 *
 * @package           widglabs
 */

if( ! defined('ABSPATH') ) {
	exit;
}

/**
 * Registers custom blocks
 *
 * @param string $block_name
 * @param array  $options
 */
function widgilabs_register_block( string $block_name, array $options = array() ) {
	register_block_type( 'widgilabs/' . $block_name,
	array_merge(
		array(
			'editor_style'  => 'widgilabs-editor-style',
			'editor_script' => 'widgilabs-editor-script',
			'style'         => 'widgilabs-style',
			'script'        => 'widgilabs-script'
		),
		$options
	));
}

/**
 * Start functions
 */
function widgilabs_blocks_register() {
	
	wp_enqueue_style(
		'widgilabs-editor-style',
		plugins_url( 'dist/editor.css', __FILE__ ),
		array('wp-edit-blocks'),
		time()
	);
	
	wp_enqueue_style(
		'widgilabs-style',
		plugins_url( 'dist/style.css', __FILE__ ),
		array(),
		time()
	);
	
	wp_register_script(
		'widgilabs-editor-script',
		plugins_url( 'dist/editor.js', __FILE__ ),
		array(
			'wp-blocks',
			'wp-i18n',
			'wp-element',
			'wp-editor',
			'wp-components',
			'wp-html-entities',
		),
		time(),
		true
	);
	
	wp_register_script(
		'widgilabs-script',
		plugins_url( 'dist/script.js', __FILE__ ),
		array( 'jquery' ),
		time(),
		true
	);
	
	widgilabs_register_block( 'events', array(
		'render_callback' => 'widgilabs_events_render_callback',
		'attributes'      => array(
			'base_api'   => array(
				'type'    => 'string',
				'default' => 'https://www.agendalx.pt/wp-json/agendalx/v1/events'
			),
			'type_event' => array(
				'type'    => 'string',
				'default' => 'all'
			),
			'event_id'   => array(
				'type'    => 'string',
				'default' => ''
			),
		)
	) );
}
add_action('init', 'widgilabs_blocks_register');

/**
 * Function render callback for events block
 *
 * @param $attributes
 *
 * @return string
 */
function widgilabs_events_render_callback( $attributes ) {
	try {
		$response = array();
		
		if( (string) $attributes['type_event'] === 'all' ):
			$response = wp_remote_get($attributes['base_api'], [
				'sslverify'           => false
			]);
		elseif( (string) $attributes['type_event'] === 'unique' ):
			$response = wp_remote_get($attributes['base_api'] . DIRECTORY_SEPARATOR . (int) $attributes['event_id'], [
				'sslverify'           => false
			]);
		endif;
		
		$responseBody   = wp_remote_retrieve_body( $response );
		$result         = json_decode( $responseBody );
		
		if( isset( $result->data->status ) && (int) $result->data->status === 404 ):
			throw new Exception($result->message, $result->data->status);
		elseif( isset( $result->code ) && (string) $result->code === 'event_not_found' ) :
			throw new Exception($result->message, 404);
		endif;
		
		$output = '';
		if( (string) $attributes['type_event'] === 'all' ):
			
			/**
			 * All Events
			 */
			$output = '<div class="widgilabs-list-events">';
			foreach ($result as $event):
				$output.= widgilabs_event_render( $event );
			endforeach;
			$output.= '</div><!-- close list events -->';
		elseif( (string) $attributes['type_event'] === 'unique' ):
			$event = $result->data;
			
			/**
			 * Singular Event
			 */
			$output = '<div class="widgilabs-list-event">';
				$output.= widgilabs_event_render( $event );
			$output.= '</div><!-- close list event -->';
		else:
			$output.= '<p>'. __('Nenhum evento a ser listado.', 'widgilabs').'</p>';
		endif;
		
		return $output;
	} catch (Exception $exception) {
		
		return "<div class='widgilabs-alert alert-danger'>" . $exception->getMessage() . "</div>";
	}
}

/**
 * Render Html card event
 *
 * @param $event
 *
 * @return string
 */
function widgilabs_event_render( $event ) {
	$output = "<div id='event-{$event->id}' class='widgilabs-event'>";
		$output.= '<div class="widgilabs-figure">';
			$output.= "<div class='widgilabs-image' style='background: url({$event->featured_media_large})no-repeat center/cover'></div>";
		$output.= '</div>';
		
		$output.= '<div class="widgilabs-content">';
			$output.='<div>';
				$output.= "<h4 class='widgilabs-title'>{$event->title->rendered}</h4>";
				$output.= "<p class='widgilabs-description'>{$event->description[0]}</p>";
			$output.='</div>';
			
			$output.='<div>';
	
				foreach ( $event->venue as $venue):
					$output.= "<p class='widgilabs-avenue'>";
						$output.= '<img src="'. plugins_url( 'images/maps.png', __FILE__ ) .'" alt="maps"> ';
						$output.= "{$venue->name}";
					$output.= "</p>";
					break;
				endforeach;
				
				$output.= "<p class='widgilabs-startdate'>";
					$output.= '<img src="'.  plugins_url( 'images/calendar.png', __FILE__ ) .'" alt="calendar"> ';
					$output.= "{$event->StartDate}";
				$output.= "</p>";
				
			$output.='</div>';
		$output.= "</div>";
	$output.= "</div>";
	
	return $output;
}

/**
 * The custom function that increases the request timeout because the API takes 8s + to response.
 *
 * @return int
 */
function widgilabs_http_request_timeout( ) {
	return 15;
}
add_filter( 'http_request_timeout', 'widgilabs_http_request_timeout' );
